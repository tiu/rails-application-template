# frozen_string_literal: true

module Helpers
  include Helpers::TextHelper
  include Helpers::TaskHelper
end
