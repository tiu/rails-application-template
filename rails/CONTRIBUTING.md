# How to contribute

We're happy to get contributions. Bug reports are helpful. Pull-requests (that solve your bug) are much more interesting, and give us more incentive to take a look.

Please understand, we're a small team supporting a large number of projects, so we can't always give your problems or good ideas the attention we would like. Outside contributions are accepted on a best-effort basis. Usually, we take a look when we're in the codebase for our own needs. That can be weeks, months...sometimes longer.

## Submitting changes

* Fork our repository.
* Make your change.
* Test your change.
* Push your changes to a topic branch in your fork of the repository.
* Submit a pull request to our repository

Next time we're working in this codebase, we'll take a look. Please understand if it takes a while.

## Sponsoring changes

Reach out if you have a change requests that you wish to sponsor.
